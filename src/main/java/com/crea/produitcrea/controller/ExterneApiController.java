package com.crea.produitcrea.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.Api;

@Api("Api pour les optérations CRUD pour l'appelle à une api externe")
@RestController
@RequestMapping("/api/call")
public class ExterneApiController {
  private String accessKey = "36f42fe2f88cd1d36e033637837f8eba";
  
  @GetMapping(value = "/countries")
  public String apiCallCountries() {
    final String uri = "http://api.countrylayer.com/v2/all?access_key=" + accessKey;
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate.getForObject(uri, String.class);
  }
 
  @GetMapping(value = "/country/{name}")
  public String apiCallContry(@PathVariable String name) {
    final String uri = "http://api.countrylayer.com/v2/name/" + name + "?access_key=" + accessKey +"&FullText=true";
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate.getForObject(uri, String.class);
  }
}
