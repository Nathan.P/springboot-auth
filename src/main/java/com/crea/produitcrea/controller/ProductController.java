package com.crea.produitcrea.controller;

import java.util.List;

import com.crea.produitcrea.dao.ProductDao;
import com.crea.produitcrea.models.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

@Api("Api pour les optérations CRUD pour le produit")
@RestController
@RequestMapping("/api/products")
public class ProductController {
  @Autowired
  private ProductDao productDao;

  @GetMapping(value = "")
  public List<Product> listProducts() {
    return productDao.findAll();
  }

  // Récupérer un produit par son Id
  @GetMapping(value = "/{id}")
  public Product displayProduct(@PathVariable int id) {
    return productDao.findById(id);
  }

  // Récupérer un produit par le prix le plus eleve
  @GetMapping(value = "/BigProduit/{id}")
  public List<Product> displayProductByPrixGreaterThan(@PathVariable int id) {
    return productDao.findByPrixGreaterThan(id);
  }

  // ajouter un produit
  @PostMapping(value = "/add")
  public void addProduct(@RequestBody Product product) {
    productDao.save(product);
  }
}
