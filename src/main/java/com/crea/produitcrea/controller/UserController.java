package com.crea.produitcrea.controller;

import java.util.ArrayList;
import java.util.List;

import com.crea.produitcrea.dao.ProductDao;
import com.crea.produitcrea.dao.UserDao;
import com.crea.produitcrea.models.Product;
import com.crea.produitcrea.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

@Api("Api pour les optérations CRUD pour les user")
@RestController
@RequestMapping("/api/users")
public class UserController {
  @Autowired
  private UserDao userDao;
  @Autowired
  private ProductDao productDao;
  @Autowired
  private PasswordEncoder bCryptPasswordEncoder;

  @GetMapping(value = "")
  public List<User> listUsers() {
    return userDao.findAll();
  }

  @GetMapping("/products")
  public List<Product> listProducts(Authentication authentication) {
    String username = authentication.getName();
    User user = userDao.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    return user.getProduct();
  }

  @PutMapping("/products/set")
  public void setProducts(Authentication authentication, @RequestBody int[] productIds) {
    String username = authentication.getName();
    User user = userDao.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    ArrayList<Product> products = new ArrayList<Product>();
    for (int productId : productIds) {
      Product product = productDao.findById(productId);
      if (product != null) {
        products.add(product);
      }
    }
    user.setProduct(products);
    userDao.save(user);
  }

  @PostMapping("/signup")
  public void signUp(@RequestBody User user) {
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    userDao.save(user);
  }
}
