package com.crea.produitcrea.dao;

import java.util.List;

import com.crea.produitcrea.models.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {

    public List<Product> findAll();

    public Product findById(int id);

    public Product save(Product p);

    public List<Product> findByPrixGreaterThan(int prix);
    
}