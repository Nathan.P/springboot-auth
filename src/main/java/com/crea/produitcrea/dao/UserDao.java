package com.crea.produitcrea.dao;

import java.util.List;

import com.crea.produitcrea.models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
  public List<User> findAll();

  public User findById(int id);
  
  public User findByUsername(String username);

  public User save(User user);
}
