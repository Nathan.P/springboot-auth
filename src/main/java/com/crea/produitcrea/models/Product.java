package com.crea.produitcrea.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String nom;
  private int prix;
  private int prixAchat;

  // constructeur par défaut
  public Product() {
  }

  public Product(int id, String nom, int prix, int prixAchat) {
    this.id = id;
    this.nom = nom;
    this.prix = prix;
    this.prixAchat = prixAchat;
  }

  // constructeur pour nos tests

  public int getPrixAchat() {
    return prixAchat;
  }

  public void setPrixAchat(int prixAchat) {
    this.prixAchat = prixAchat;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public int getPrix() {
    return prix;
  }

  public void setPrix(int prix) {
    this.prix = prix;
  }

  @Override
  public String toString() {
    return "Product{" + "id=" + id + ", nom='" + nom + '\'' + ", prix=" + prix + '}';
  }
}